import react from "react";
import { Row, Col, Card } from "react-bootstrap";
import classes from './NameCard.module.css';


const NameCard = (props) => {
    return (
        <Row className="m-0" onClick={() => props.handleCardClick(props.id)}>
            <Col sm={{ span: 8, offset: 2 }}>
                <Card className={classes.namecard}>
                    <Row className="m-0">
                        <Col xs="12" sm="6">
                            <p> <b> Name: </b> <i> {props.name} </i> </p>
                        </Col>
                        <Col xs="12" sm="6">
                            <p className="text-sm-end"> <b> Posts: </b> {props.postCount} </p>
                        </Col>
                    </Row>
                </Card>
            </Col>
        </Row>
    );
};

export default NameCard;
