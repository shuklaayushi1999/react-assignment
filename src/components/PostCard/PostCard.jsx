import react, { useState } from "react";
import { Row, Col, Card, Modal } from "react-bootstrap";
import classes from './PostCard.module.css';


const PostCard = (props) => {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Card className={classes.postcard} onClick={handleShow}>
                <Row className="m-0">
                    <Col xs="12">
                        <h4 className="text-center mb-3"> {props.title} </h4>
                    </Col>
                    <Col xs="12">
                        <p> {props.description} </p>
                    </Col>
                </Row>
            </Card>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>{props.description}</Modal.Body>
            </Modal>
        </>
    );
};

export default PostCard;
