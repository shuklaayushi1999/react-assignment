import react from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Card, Button } from "react-bootstrap";
import classes from './Profile.module.css';
import { removeUser } from "../../store/userprofile-slice";
import PostCard from "../PostCard/PostCard";

const Profile = () => {

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const userProfile = useSelector((state) => state.userProfile.user);

    const handleBack = () => {
        dispatch(removeUser());
        navigate('/');
    }

    return (

        <>
            <header className={classes.header}>
                <Button className={`${classes.backbutton} align-self-start`} onClick={handleBack}>
                    Back
                </Button>

                <h4> {userProfile.name}'s  Profile</h4>
            </header>

            <div className="container py-4">
                <Row className="m-0 my-5">
                    <Col sm="12">
                        <Card className="p-3">
                            <Row className="m-0">
                                <Col xs="12" sm="6">
                                    <p className={classes.boldtext}> {userProfile.name} </p>
                                    <p className={classes.smalltext}> {userProfile.username} | {userProfile.catchPhrase} </p>
                                </Col>
                                <Col xs="12" sm="6">
                                    <p style={{fontSize: '1rem'}} className="text-sm-end"> {userProfile.address} </p>
                                    <p className={`${classes.smalltext} text-sm-end` }> {userProfile.email} | {userProfile.contact} </p>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    {
                        !!userProfile.posts && userProfile.posts.map((item, index) => {
                            return (
                                <Col xs="12" md="6" lg="4" key={index}>
                                    <PostCard title={item.title} description={item.description} />
                                </Col>
                            )
                        })
                    }
                </Row>
            </div>
        </>
    );
};

export default Profile;
