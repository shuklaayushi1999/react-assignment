import react from "react";
import { Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import NameCard from "../NameCard/NameCard";
import classes from './ProfileList.module.css';
import { useNavigate } from "react-router";
import { addUser } from "../../store/userprofile-slice";

const ProfileList = () => {

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const profiles = useSelector((state) => state.profile.profiles);

    const handleCardClick = (id) => {

      let user = profiles.filter((e) => { return e.id == id });

      dispatch(addUser(user[0]));
      navigate(`/profile/${id}`);
    }

    // const profiles = useSelector((state) => state.profiles);
    return (

        <>
            <header className={classes.header}>
                <h4>Profile Directory</h4>
            </header>

            <div className="container py-4">
                <Row>
                    {!!profiles && profiles.map((card, index) => {
                        return (
                            <NameCard key={index} id={card.id} name={card.name} postCount={card.postCount} 
                                handleCardClick={handleCardClick}
                            />
                        )
                    })}
                </Row>
            </div>
        </>
    );
};

export default ProfileList;
