import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Profile from "./components/Profile/Profile";
import ProfileList from "./components/ProfileList/ProfileList";

export default function App() {

  const router = createBrowserRouter([
    {
      path: "/",
      element: <ProfileList />
    },
    {
      path: "/profile/:id",
      element: <Profile />
    }
  ]);

  return (
      <div className="App">
        <RouterProvider router={router}> </RouterProvider>
      </div>
  );
}
