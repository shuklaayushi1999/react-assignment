import { createSlice } from '@reduxjs/toolkit';

const userprofileSlice = createSlice({
    name: 'userProfile',
    initialState: { user: {} },
    reducers: {
        addUser: (state, action) => {
            state.user = action.payload;
        },
        removeUser: (state) => {
            state.user = null;
        }
    }
});

export const { addUser, removeUser } = userprofileSlice.actions;
export default userprofileSlice;