import { createSlice } from '@reduxjs/toolkit';

const profiles = [
    { 
        id: 1, name: 'Ayushi Shukla', postCount: 3, username: '@yushi', catchPhrase: 'Software Engineer',
        email: 'ayushi@gmail.com', address: 'Indore', contact: 8718059784,
        posts: [
            {
                title: 'Post 1',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere commodo nibh at gravida. Nam ut ipsum sed lectus egestas eleifend fermentum a turpis.'
            },
            {
                title: 'Post 2',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere commodo nibh at gravida. Nam ut ipsum sed lectus egestas eleifend fermentum a turpis.'
            },
            {
                title: 'Post 3',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere commodo nibh at gravida. Nam ut ipsum sed lectus egestas eleifend fermentum a turpis.'
            }
        ] 
    }, 
    { 
        id: 1, name: 'Ayushi', postCount: 2, username: '@yushis', catchPhrase: 'Frontend Architect', 
        email: 'ayushi.shukla@gmail.com', address: 'Indore', contact: 8718057784,
        posts: [
            {
                title: 'Post 1',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere commodo nibh at gravida. Nam ut ipsum sed lectus egestas eleifend fermentum a turpis.'
            },
            {
                title: 'Post 2',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere commodo nibh at gravida. Nam ut ipsum sed lectus egestas eleifend fermentum a turpis.'
            }
        ] 
    },
    { 
        id: 1, name: 'Shukla Ayushi', postCount: 1, username: '@yushii',  catchPhrase: 'Javascript Developer',
        email: 'ayushi22@gmail.com', address: 'Indore', contact: 8718759784,
        posts: [
            {
                title: 'Post 1',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere commodo nibh at gravida. Nam ut ipsum sed lectus egestas eleifend fermentum a turpis.'
            }
        ] 
    },
];

const profileSlice = createSlice({
    name: 'profile',
    initialState: { profiles: profiles },
    reducers: {}
});

export default profileSlice;