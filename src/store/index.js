import { configureStore } from "@reduxjs/toolkit";
import profile from "./profile-slice";
import userProfile from "./userprofile-slice";

const store = configureStore({
    reducer: { 
        profile: profile.reducer,
        userProfile: userProfile.reducer
    }
});

export default store;